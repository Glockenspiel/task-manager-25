# TASK MANAGER

## DEVELOPER INFO

**NAME:** Victoria Sukhorukova

**E-MAIL:** vsukhorukova@t1-consulting.ru

**E-MAIL:** murrika018@mail.ru

## SOFTWARE

**JAVA:** OPENJDK 1.8

**OS:** Windows 10, version 1809

## HARDWARE

**CPU:** i5

**RAM:** 16GB

**SSD:** 512GB

## BUILD PROGRAM

```
mvn clean install
```

## RUN PROGRAM

```
java -jar ./task-manager.jar
```
