package ru.t1.sukhorukova.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Setter @Getter
public abstract class AbstractUserOwnerModel extends AbstractModel {

    @Nullable private String userId;

}
